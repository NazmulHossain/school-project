const mongoose = require('mongoose');


// Schema
const Schema = mongoose.Schema;
const BlogPostSchema = new Schema({
    question: String,
    option01: String,
    option02: String,
    option03: String,
    option04: String,
    answer: String,
    date: {
        type: String,
        default: Date.now()
    }
});

// Model
const BlogPost = mongoose.model('quizzes', BlogPostSchema);

module.exports =  BlogPost;