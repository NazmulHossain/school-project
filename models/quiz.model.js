const mongoose = require('mongoose');

var quizSchema = new mongoose.Schema({
    question: {
        type: String,
        required: 'This field is required.'
    },
    option01: {
        type: String,
        required: 'This field is required.'
    },
    option02: {
        type: String,
        required: 'This field is required.'
    },
    option03: {
        type: String,
        required: 'This field is required.'
    },

    option04: {
        type: String,
        required: 'This field is required.'
    },
    answer: {
        type: String,
        required: 'This field is required.'
    },

});



mongoose.model('Quiz', quizSchema);