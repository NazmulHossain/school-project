const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI ||'mongodb://127.0.0.1:27017/postManagerDB', { useNewUrlParser: true , useUnifiedTopology: true}, (err) => {
    if (!err) { console.log('MongoDB Connection Succeeded.') }
    else { console.log('Error in DB connection : ' + err) }
});

require('./quiz.model');
require('../blogPost')