require('./models/db');
const morgan = require('morgan');
const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const bodyparser = require('body-parser');

const quizController = require('./controllers/quizController');
// const BlogPost= require('./controllers/quizController')
const api= require('./api')

var app = express();
app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(bodyparser.json());
app.set('views', path.join(__dirname, '/views/'));
app.engine('hbs', exphbs({ extname: 'hbs', defaultLayout: 'mainLayout', layoutsDir: __dirname + '/views/layouts/' }));
app.set('view engine', 'hbs');

app.listen(8080, () => {
    console.log('Express server started at port : 8080');
});
app.use(morgan('tiny'));
app.use('/quiz', quizController);
app.use('/demo',api)
